# Go

Interesting stuff that I've learned...

## Interfaces.

Interfaces are awesome, they let you abstract the shape
of types to provide mutability, it's like OOP but better,
because it allows similar-interface forms to be received
as fixed parammeters of functions, here's a good example:

Here's a good example of them: [JSON-RPC: a tale of interfaces](http://blog.golang.org/2010/04/json-rpc-tale-of-interfaces.html)

## Testing stuff.

For some reason I thought that the testing stuff had to have
a different package name in the first line, but it doesn't,
it's meant to be used within the local package, so it takes
it functions, methods and variables as if they were locally.

## Binary types.

Ints are not binary types but they can be used to perform
binary operations (unlike strings), it's even useful to use
`var x = iota` to hold a bunch of int variables together
and later use them as binary data.

Also, Go has globally type functions (such as `byte()` and `uint64()`).
You can only use most of them with limited types (byte uses int),
but you can compare them with `==` to hexadecimal values, as well
as perform binary operations directly with hex values (`0x00`).

