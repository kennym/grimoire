# Vim Diaries


## [Working with vim sessions](#sessions)

Make a session with the current oppened files with:

    :mksession! ~/code/bitbucket/data/go/Session.vim

(Remember, the exclamation sign is to allow rewriting the existing file)

Then you'll be able to load it.
Open vim, then type:

    :source ~/code/bitbucket/data/go/Session.vim

That's it!

[Source](http://vim.runpaint.org/editing/managing-sessions/)


## [LookAhead and LookBehind in VIM](#lookbehind)

-   **LookBehind**  : `/\(Start\)\@<=Date` Matches 'Date'  in 'StartDate' but not in 'EndDate' and 'YesterdaysDate'
-   **!LookBehind** : `/\(Start\)\@<!Date` Matches 'Date'  in 'EndDate'   but not in 'StartDate'
-   **LookAhead**   : `/Start\(Date\)\@=`  Matches 'Start' in 'StartDate' but not in 'Starting'
-   **!LookAhead**  : `/Start\(Date\)\@!`  Matches 'Start' in 'Starting   but not in 'StartDate'

[Source](http://ssiaf.blogspot.com/2009/07/negative-lookbehind-in-vim.html)  
[More Regex Links](http://vimregex.com/)

