## Paste from old yanks

>   All text cut and copy operations are saved into registers.
    If you have cut text and then cut something else, you haven’t
    lost the first cut – just type :reg (short for :registers)
    to see all of the registers. You can then type the name of
    the register and then p, such as "3p, to paste whatever
    you cut three or four operations prior.

[Source](http://statico.github.com/vim.html)
