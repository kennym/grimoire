# VIM

## Write the same in multiple lines.

Enter in the _block_ visual mode with `<C-v>`,
select the lines you want (like, going down),
then press `<I>`, write what you want,
press `<ESC>` and press any move key.
