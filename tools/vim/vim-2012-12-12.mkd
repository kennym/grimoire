# VIM

## [Yank to Xclip](#xclip)

If you want to copy stuff from vim to xclip you
have to do the next:

    :call system('xclip', @0)

And if you want to paste stuff from
xclip to vim, you can do as follows:

    :r!xclip -o

Both can be beautifully combined in maps :3

    nmap xc :call system('xclip', @0)<CR>
    nmap xv :r!xclip -o<CR>

