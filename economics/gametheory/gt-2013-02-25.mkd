# Game Theory - Coalitional Games

-   What is fair?

## The Shapley Value

-   Lloyd Shapley's Idea:
    Members should receive
    payments or shares
    proportional to their
    marginal contribution.

But:
-   Everyone has to be there?
    Suppose `v(n) = 1`
    but `v(S) = 0 if N ≠ S`.
-   Then `v(N) - v(N \ {i}) = 1`
    for every `i`:
    everybody's marginal contribution is 1,
    everybody is essential to generate any value.
-   Cannot pay everyone their marginal contribution!
-   We need a weighting system.

## Symmetry

-   `i` and `j` are interchangeable relative to `v`
    if they always contribute the same amount
    to every other coalition.
    -   For all `S` that contains either `i` nor `j`
        `v(S ∪ {i}) = v(S ∪ {i})`.
-   Interchangeable agents should receive
    the same shares/payments.

## Dummy Player

-   Their contribution to any coalition is 0.
    `for all S: v(S ∪ {i}) = v(S)`

## Additivity

-   For any two `v1` and `v2`
    the allocation of the sum of both should be equal
    to the sum of both allocations.
    `Ui(N, v1 + v2) = Ui(N,v1) + Ui(N, v2)`
    Where the game `(N, v1 + v2)` is defined by
    `(v1 + v2)(S) = v1(S) + v2(S)`
    for every coalition `S`.


## Shapley Value

Given a coalitional game `(N, v)`, there is a qunique
payoff division `x(v) = ø(N, v)` that divides the full
payoff of the grande coalition and that satisfis the
Symmetry, Dummy Player and Additivity axioms:
The Shapley Value.

It captures the marginal contributions of
agent `i`, averaging over all the different
sequences according to which the grand
coalition could be built up.

Coalitions can form in many different ways:

    1   13  123
    2   23  123

Steps:
-   Calculate along the sequence
    what values should be added
    `[v(S ∪ {i}) - v(S)]`.
-   Then we weight it by the total ways
    the set `S` could have been formed: `|S|!`.
-   Also weighted by: `(|N| - |S| - 1)!`
    the ways the remaining players could be added.
-   Sum over all possile sets `S`...
-   Divided by the total number of orders we could have
    `1/|N|!`.

Example:

    v({1}) = 1
    v({2}) = 2
    v({1,2}) = 4

For player one:

    1   12  v(1) = 1
    2   12  v(12) - v(2) = 2
    
    (1 * 1/2) + (2 * 1/2) = 1.5

Player 1 gets: `1.5 = ø1`
Player 2 gets: `2.5 = ø2`


Aand... 20 hours later!

## Stable payoff division

-   Shapley ignored stability.
-   Would the agents be willing
    to form the grand coalition given
    the way it will divide payments,
    or would some of them prefer
    to form smaller coalitions?
    -   Smaller coalitions can be attractive
        even if they lead to lower value overall.

-   They would want to agree if and only if
    the payment profile is drawn from a set
    called the core.

## The Core

Definition:

    ∀S ⊆ N, sum(i∈S, xi) ≥ v(S)

-   The sum of payoff to the agents
    in any subcoalition S is at least
    as much as they could earn on their own.

-   Like Nash Equilibrium but
    it allows deviations by groups.

-   Agents don't have any proffitable deviation.

-   Considerations:
    -   The core is not always nonempty.
        -   The example game,
            the players will to
            get in the circle
            with the less they could get.
        -   There's always a way a subcoalition
            could form.
        -   Thus, the core is empty.
    -   The core is also not always unique.
        -   In the example game,
            in any form of core
            didn't envolve using always
            a the most important players
            (we could use A with C or D),
            it would not be empty if
            we needed to use A and B
            to get the voters (the'd need to be 80 voters).

Two other definitions:
-   Simple game:  
    A game `G = (N, v)` is simple
    if for all `S ⊂ N`, `v(S) ∈ {0,1}`
    v is either 0 or 1.
-   Veto player:  
    A player `i` is a veto player
    if `v(N \ {i}) = 0`
    Meaning: the value of all coalitions
    that don't envolve this player is 0.

The core is empty
if there's no veto players.

-   Convex Games:
    A game `G = (N, v)` is convex
    if for all `S, T ⊂ N`,
    `v(S ∪ T) ≥ v(S) + v(T) - v(S ∩ T)`.
    -   Convexity is stronger than superadditivity.
    -   Airport game is convex.
    -   Every convex game is nonempty.
    -   In every convex game the Shapley value is in the core.


## Example

Suppose N=3 and v(1)=v(2)=v(3)=0, v(1,2)=v(2,3)=v(3,1)=2/3, v(1,2,3)=1.
Which allocation is in the core of this coalitional game?

R: (1/3, 1/3, 1/3); 

-   By definition, the core of this game is formed by a triplet (x1,x2,x3)∈R+3 that satisfies:

        xi+xj≥2/3 for i≠j
        x1+x2+x3≥1
        Then, the core is a singleton with (x1,x2,x3)=(1/3,1/3,1/3).

---

## Note

Wikipedia's way to handle the table
is quite intuitive!

[Shapley value at Wikipedia](http://en.wikipedia.org/wiki/Shapley_value)

