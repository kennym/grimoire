# Game Theory

Summary...

## Best responses

-   Are those the player would not want to deviate.
-   First you pick a possible response, then you ask
    yourself: Do I want to deviate?
    If no, then it's a best response.

## Pure Strategy

-   Pure strategies mean all the possible strategies for each player.

## Pure Strategy Nash Equilibrium

-   None of the players want to deviate once
    they picked the strategy.
-   So you first start with one cell and then
    you ask yourselve: Would I, as player n, would like to deviate?
    If you think "no" for both players, then it's a pure nash eq.

## Pareto Optimality

-   _o_ pareto dominates _o'_ if
    they're the same for everyone
    but there's someone that likes _o_
    most by no perceivable reason.
-   An outcome _o*_ is Pareto-optimal
    if there's no other outcome that
    pareto-dominates it.
-   _Prisoner's dilema_:
    The nash equilibrium of that game
    is the only non-Pareto-optimal outcome.

## Mixed Strategies

-   Pure strategy: Only one action is played with positive probability.
-   Mixed strategy: More than one action is played with positive probability.
-   Expected utility from decisition theory: `for a in A { sum += u(a[i]) Pr(a|s) }`.
-   Player 1 must randomize to make player 2 indifferent.
    So, take player 2 outcomes based on player 2 actions.

              B       F
           ______  ______
        B [ 2, 1 ][ 0, 0 ]
        F [ 0, 0 ][ 1, 2 ]
           ~~~~~~  ~~~~~~
               ↑       ↑
        
               u2(B) = u2(F)
        q + 0(1 - q) = 0q + 2(1 - q)
                   q = 2(1 - q)
                   q = 2 - 2q
                  3q = 2
                   q = 2/3

## Iterative removal

-   Strictly dominated strategy can never be a best reply.
-   Iterate over this.
-   So for player two, the dominance of his strategies
    comes from his expected outputs (not player one's)

              L       C       R
           ______  ______  ______
        U [ 3, 0 ][ 2, 1 ][ 0, 0 ]
        M [ 1, 1 ][ 1, 1 ][ 5, 0 ]
        D [ 0, 1 ][ 4, 2 ][ 0, 1 ]
           ~~~~~~  ~~~~~~  ~~~~~~
               ↑       ↑       ↑
                       C   >   R
        
                       R is strictly dominated by C.
                       (C and R are player2's strategies
                       so we count only his outcomes).

-   On pure strategies, order of removal doesn't matter.
-   On mixed strategies, order does matter.

## Maxmin Strategies

-   Maxmin strategy is one that maximizes
    the worst-case payoff
-   Maxmin value is the minimum payoff
    guaranteed by maxmin strategies.
-   _Maxmin_ is defensive.
-   _Minmax_ is offensive.
-   Minmax is to minimizes enemies'
    best-case payoff.
-   In a finite, two-player, zero-sum game,
    Nash Equilibrium preserves minmax and maxmin.
-   To calculate...  
    Estimates the best response
    for each possible action,
    then calculate the variables.

## Perfect Information Extensive Form: Strategies

-   In the extensive form, every node of possible strategies can be
    deduced as a bit in an n-bit string, so the number of possible
    actions by users is `pow(base, nnodes)`.


