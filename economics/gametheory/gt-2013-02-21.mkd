# Game Theory

## Discounted Repeated Games

-   Things are decaying exponentially in terms of discount.
-   Stage game: Normal form game over time.
-   They care about the future but they care more today
    than tomorrow.
-   Histories of length: `t: H^t = { h^t : h^t = (a^1, ..., a^t) ∈ A^t }`
-   Finite histories: `H = t∪^T`

-   Repeatedly playing a Nash Equilibrium of the stage game
    is always a subgame perfect equilibrium of the repeated
    game.

-   In the Prisoner's Dilema.
-   Compare full cooperate, long termn,
    to full defect, long termn.
-   The difference between both
    is the real factor to analyze.

-   The n of elements in the set of history per player is:
    all the elements in the table (in the example: 9).
    For two players is that number squared (9^2).


