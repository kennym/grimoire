# Mathematics

## Intuitive quotes:

1.  About the relation between integers, rational numbers and groups:

    > If you want to measure and describe the number of occurrences of
    > something, use an integer. If you want to measure and describe the amount
    > of something, use a rational number. If you want to measure and describe
    > the symmetry of a structure, use a group.

## Good resources:

-   [Intro to Group Theory](http://ureddit.com/class/23794/intro-to-group-theory)
-   [Abstract Algebra: Theory and Applications](http://abstract.ups.edu/)
-   [Algebraic Set Theory](http://www.phil.cmu.edu/projects/ast/)

