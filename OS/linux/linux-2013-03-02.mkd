# Linux

## cmus

-   [Website](http://cmus.sourceforge.net/)
-   [cmus tutorial at ubuntu manpages](http://manpages.ubuntu.com/manpages/precise/man7/cmus-tutorial.7.html)
-   [cmus tutorial at arch's wiki](https://wiki.archlinux.org/index.php/Cmus)
-   [cmus tutorial at tuxarena](http://www.tuxarena.com/static/cmus_guide.php)


### Tutorial

-   Add music: `:add ~/music`
-   Clear: `:clear`
-   Toggle shuffle: `s`

The tutorials have the full list of things,
for quick access to the player stuff, type `:player-` and press tab.


