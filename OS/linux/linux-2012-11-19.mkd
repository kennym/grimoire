# Linux Diaries


## [Find and play files with MPlayer](#find-play-mplayer)

Sometimes we want just to hear a bunch of files in a directory,
with mplayer this is easily achieved this way:

    find ~/music/artist > /tmp/play ; mplayer -playlist /tmp/play

If you want to play all the files in a directory
with a certain word in teir names, you can do:

    find ~/music | grep artist > /tmp/play ; mplayer -playlist /tmp/play

:P awesome.


## [Find Files by Date](#find-by-date)

mtime +60 means you are looking for a file modified 60 days ago.
mtime -60 means less than 60 days.
mtime 60 If you skip + or - it means exactly 60 days.

    find ./ -mtime -60 -type f

[Source](http://www.cyberciti.biz/faq/howto-finding-files-by-date/)
