#!/bin/bash
cp /etc/resolv.conf /usr/local/chroots/arch/etc/
sudo mount -o bind /dev /usr/local/chroots/arch/dev
sudo mount -t devpts none /usr/local/chroots/arch/dev/pts
sudo mount -t proc proc /usr/local/chroots/arch/proc
sudo mount -t sysfs sys /usr/local/chroots/arch/sys
sudo chroot /usr/local/chroots/arch /bin/bash 
sudo umount /usr/local/chroots/arch/dev/pts
sudo umount /usr/local/chroots/arch/dev
sudo umount /usr/local/chroots/arch/proc
sudo umount /usr/local/chroots/arch/sys
